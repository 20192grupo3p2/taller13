all: bin/cliente bin/servidor

bin/cliente: obj/cliente.o
	gcc obj/cliente.o -o bin/cliente

bin/servidor: obj/servidor.o
	gcc obj/servidor.o -o bin/servidor

obj/cliente.o: src/cliente.c
	gcc -Wall -c src/cliente.c -o obj/cliente.o

obj/servidor.o: src/servidor.c
	gcc -Wall -c src/servidor.c -o obj/servidor.o

.PHONY: clean
clean:
	rm bin/* obj/*