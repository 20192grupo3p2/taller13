
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define BUFLEN 128
#define QLEN 10
#define BUFLEN1 5000

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif



void set_cloexec(int fd){
	if(fcntl(fd, F_SETFD, fcntl(fd, F_GETFD) | FD_CLOEXEC) < 0){
		printf("error al establecer la bandera FD_CLOEXEC\n");
	}
}


//Funcion para inicializar el servidor
int levantarserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){

	int fd;
	int err = 0;

	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;

	if(bind(fd, addr, alen) < 0)
		goto errout;

	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){

		if(listen(fd, qlen) < 0)
			goto errout;
	}
	return fd;
	errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}


/// Servicio Levantado



int main( int argc, char *argv[]) {
	struct stat datos_archivo;

	int sockfd;

	if(argc == 1){
		printf("Uso: ./servidor <numero de puerto>\n");
		exit(-1);
	}

	if(argc != 3){
		printf( "por favor especificar un puerto\n");
	}

	int puerto = atoi(argv[2]);


	//Direccion del servidor
	struct sockaddr_in sock_servidor;

	memset(&sock_servidor, 0, sizeof(sock_servidor));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	sock_servidor.sin_family = AF_INET;		//IPv4
	sock_servidor.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	sock_servidor.sin_addr.s_addr = inet_addr(argv[1]) ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces



	//inicalizamos servidor (AF_INET + SOCK_STREAM = TCP)
	if( (sockfd = levantarserver(SOCK_STREAM, (struct sockaddr *)&sock_servidor, sizeof(sock_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola
		printf("Error iniciando servidor\n");
	}
	int clfd1 = accept(sockfd, NULL, NULL);
	int n = 0;
	char buf[BUFLEN] = {0};
	if (( n = recv( clfd1, buf, BUFLEN,0)) < 0) {
		printf("error al leer");

	}


	char *ptr = strtok(buf, ",");
	int x=0;
	while(x<1){
		ptr = strtok(NULL, ",");
		x++;
	}
	int res = stat(ptr, &datos_archivo);
	if(res<0){
		perror("Error stat\n");
	}
	//unsigned long size_file = datos_archivo.st_size;
	char valor[20]= {0};
	snprintf(valor, 20, "%lu",datos_archivo.st_size);
	send(clfd1, valor,20,0);
	int b;
	//ssize_t can;
	unsigned char buffer[BUFLEN1]={0};
	int f1 = open(ptr, O_RDONLY, 0444);
	while((b=read(f1,buffer,BUFLEN1))>0){
		send(clfd1, buffer, b, 0);
	}
	close(f1);
	close(clfd1);

	exit( 1);
}
