#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>


#define BUFLEN 128
#define MAXSLEEP 64
#define BUFLEN1 5000

int verify_connect( int domain, int type, int protocol, const struct sockaddr *addr, socklen_t alen){

	int numsec, fd; /* * Try to connect with exponential backoff. */

	for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) {

		if (( fd = socket( domain, type, protocol)) < 0)
			return(-1);

		if (connect( fd, addr, alen) == 0) { /* * Conexión aceptada. */
			return(fd);
		}
		close(fd); 				//Si falla conexion cerramos y creamos nuevo socket

		/* * Delay before trying again. */
		if (numsec <= MAXSLEEP/2)

			sleep( numsec);
	}
	return(-1);
}


int main( int argc, char *argv[]) {

	int sockfd;

	if(argc == 1){
		printf("Uso: ./cliente <ip> <puerto>\n");
		exit(-1);
	}

	if(argc != 5){
		printf( "informacion insuficiente");
	}

	int puerto = atoi(argv[2]);


	//Direccion del servidor
	struct sockaddr_in direccion_cliente;

	memset(&direccion_cliente, 0, sizeof(direccion_cliente));	//ponemos en 0 la estructura direccion_servidor

	//llenar los campos
	direccion_cliente.sin_family = AF_INET;		//IPv4
	direccion_cliente.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_cliente.sin_addr.s_addr = inet_addr(argv[1]) ;	//Nos tratamos de conectar a esta direccion

	//AF_INET + SOCK_STREAM = TCP

	if (( sockfd = verify_connect( direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente))) < 0) {
		printf("Conexion fallida\n");
		exit(-1);
	}

	//En este punto ya existe una conexión válida

	//int n = 0;
	char get[]="GET,";

	//int dest;

	char buf[BUFLEN1] =  {0};
	send(sockfd, strcat(get,argv[3]), strlen(get)+strlen(argv[3]), 0);
	char sizefile[20]= {0};
	recv(sockfd, sizefile, 20, 0);
	int sizef = atoi(sizefile);
	printf("%d\n",sizef);
	int fd;
	int b;
	fd = open(argv[4], O_CREAT | O_WRONLY, 	0666);
	int count = 0;
	if(fd>0){
		while((b=recv(sockfd, buf, BUFLEN1, 0))>0){

			count = count +b;
			write(fd, buf,b);
		}
	}
	if(sizef==count){
		printf("Tamaño del archivo recibido : %d\t, Total de bytes recibidos: %d\n",sizef,count);
	}
	close(fd);
	close(sockfd);

	return 0;
}